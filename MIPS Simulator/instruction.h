//
//  instruction.h
//  MIPS Simulator
//
//  Created by Christopher Clarke on 3/8/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

/***
 * TODO: Implement bitwise NOT pseudoinstruction with function code 0x5 (not rdest(rd), rsrc(rs) pseudoinstruction)
 * Implement cmp rs, rt (function code: oxe) & cmpi rs, imm (opcode 0x13), bswap rd, rs function code: 0x14 
 */

#ifndef instruction_h
#define instruction_h
#include <stdbool.h>
#include "register.h"
#define int32_t int

typedef struct r_type {
    int opcode;
    int rs;
    int rt;
    int rd;
    int shift_amount;
    int function;
}r_type;

typedef struct i_type {
    int opcode;
    int rs;
    int rt;
    int imm;
}i_type;

typedef struct j_type {
    int opcode;
    int target_address;
}j_type;

typedef struct pseudo_type {
    int opcode;
    int rs;
    int rt;
    int rd;
    int extra_space;
    int function;
}pseudo_type;

int decodeInstruction(int hex);
int get_rs(int hex);
int get_rt(int hex);
int get_rd(int hex);
int get_imm(int hex);
int get_function(int hex);
int get_target_address(int hex);
int get_shift_amount(int hex);
r_type* load_r_type(int hex);
i_type* load_i_type(int hex);
j_type* load_j_type(int hex);
bool load_pseudo_type();
bool system_call(register_file*, FILE* file);
bool add_with_overflow(r_type*, register_file*);
bool add_without_overflow(r_type*, register_file*);
bool add_immediate_with_overflow(i_type*, register_file*);
bool add_immediate_without_overflow(i_type*, register_file*);
bool AND(r_type*, register_file*);
bool and_immediate(i_type*, register_file*);
bool divide_with_overflow(r_type*, register_file*);
bool divide_without_overflow(r_type*, register_file*);
// 3 register pseudo divide 
bool multiply(r_type*, register_file*);
bool unsigned_multiply(r_type*, register_file*);
//bool multiply_with_overflow(pseudo_type); // mulo
bool multiply_without_overflow(r_type*, register_file*);
//bool negate_value_with_overflow(pseudo_type); // neg
//bool negate_value_without_overflow(pseudo_type); // negu
bool nor(r_type*, register_file*);
//bool NOT(r_type);
bool OR(r_type*, register_file*);
bool or_immediate(i_type*, register_file*);
//bool REMAINDER(pseudo_type);
//bool unsigned_remainder(pseudo_type);
bool shift_left_logical(r_type*, register_file*);
bool shift_left_logical_variable(r_type*, register_file*);
bool shift_right_arithmetic(r_type*, register_file*);
bool shift_right_arithmetic_variable(r_type*, register_file*);
bool shift_right_logical(r_type*, register_file*);
bool shift_right_logical_variable(r_type*, register_file*);
//bool rotate_left(r_type);
//bool rotate_right(r_type);
bool subtract_with_overflow(r_type*, register_file*);
bool subtract_without_overflow(r_type*, register_file*);
bool Xor(r_type*, register_file*);
bool Xor_immediate(i_type*, register_file*);
bool load_upper_immediate(i_type*, register_file*);
//bool load_immediate(pseudo_type);
bool set_less_than(r_type*, register_file*);
bool set_less_than_unsigned(r_type*, register_file*);
bool set_less_than_immediate(i_type*, register_file*);
bool set_less_than_unsigned_immediate(i_type*, register_file*);
//bool set_equal(pseudo_type);
//bool set_greater_than_equal(pseudo_type);
//bool set_greater_than_equal_unsigned(pseudo_type);
//bool set_greater_than(pseudo_type);
//bool set_greater_than_unsigned(pseudo_type);
//bool set_less_than_equal(pseudo_type);
//bool set_less_than_equal_unsigned(pseudo_type);
//bool set_not_equal(pseudo_type);
bool branch_on_equal(i_type*, register_file*);
bool branch_on_greater_than_equal_zero(i_type*, register_file*);
bool branch_on_greater_than_zero(i_type*, register_file*);
bool branch_on_less_than_zero(i_type*, register_file*);
bool branch_on_not_equal(i_type*, register_file*);
bool bswap_mips(r_type*, register_file*);
bool cmp(r_type*, register_file*);
bool cmpi(i_type*, register_file*);
//bool branch_on_equal_zero(i_type);
bool jump(j_type*, register_file*);
bool jump_and_link(j_type*, register_file*);
bool jump_register(r_type*, register_file*);
//bool load_address(i_type*, register_file*);
bool load_byte(i_type*, register_file*, memory*);
bool load_unsigned_byte(i_type*, register_file*, memory*);
bool load_half_word(i_type*, register_file*, memory*);
bool load_unsigned_half_word(i_type*, register_file*, memory*);
bool load_word(i_type*, register_file*, memory*);
//bool load_double_word(i_type);
bool store_byte(i_type*, register_file*, memory*);
bool store_half_word(i_type*, register_file*, memory*);
bool store_word(i_type*, register_file*, memory*);
//bool store_double_word(i_type);


#endif /* instruction_h */
