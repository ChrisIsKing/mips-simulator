//
//  main.c
//  MIPS Simulator
//
//  Created by Christopher Clarke on 3/8/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

#include <stdio.h>
#include "instruction.h"
#include "register.h"
#include <stdlib.h>
#include <string.h>

#define int32_t int

register_file* Register;
memory* Memory;
bool (*r_type_function[64])(r_type*, register_file*);
bool (*i_type_register_function[64])(i_type*, register_file*);
bool (*i_type_memory_function[64])(i_type*, register_file*, memory*);

int main(int argc, char* argv[]) {
    
    // loads hex files
    FILE* file = fopen(argv[1], "r");
    if (file == NULL) {
        exit(10);
    }
    int hex[100] = {0};
    int i = 0;
    while (fscanf(file, "%x\n", &hex[i]) != EOF) {
        i++;
    }
    
    // load r type functions
    r_type_function[0] = shift_left_logical;
    r_type_function[2] = shift_right_logical;
    r_type_function[3] = shift_right_arithmetic;
    r_type_function[4] = shift_left_logical_variable;
    r_type_function[6] = shift_right_logical_variable;
    r_type_function[7] = shift_right_arithmetic_variable;
    r_type_function[8] = jump_register;
    r_type_function[14] = cmp;
    r_type_function[20] = bswap_mips;
    r_type_function[24] = multiply;
    r_type_function[25] = unsigned_multiply;
    r_type_function[26] = divide_with_overflow;
    r_type_function[27] = divide_without_overflow;
    r_type_function[28] = multiply_without_overflow;
    r_type_function[32] = add_with_overflow;
    r_type_function[33] = add_without_overflow;
    r_type_function[34] = subtract_with_overflow;
    r_type_function[35] = subtract_without_overflow;
    r_type_function[36] = AND;
    r_type_function[37] = OR;
    r_type_function[38] = Xor;
    r_type_function[39] = nor;
    r_type_function[42] = set_less_than;
    r_type_function[43] = set_less_than_unsigned;
    
    // load i type register functions
    i_type_register_function[4] = branch_on_equal;
    i_type_register_function[5] = branch_on_not_equal;
    i_type_register_function[7] = branch_on_greater_than_zero;
    i_type_register_function[8] = add_immediate_with_overflow;
    i_type_register_function[9] = add_immediate_without_overflow;
    i_type_register_function[10] = set_less_than_immediate;
    i_type_register_function[11] = set_less_than_unsigned_immediate;
    i_type_register_function[12] = and_immediate;
    i_type_register_function[13] = or_immediate;
    i_type_register_function[14] = Xor_immediate;
    i_type_register_function[19] = cmpi;
    
    // load i type memory functions
    i_type_memory_function[32] = load_byte;
    i_type_memory_function[33] = load_half_word;
    i_type_memory_function[35] = load_word;
    i_type_memory_function[36] = load_unsigned_byte;
    i_type_memory_function[37] = load_unsigned_half_word;
    i_type_memory_function[40] = store_byte;
    i_type_memory_function[41] = store_half_word;
    i_type_memory_function[43] = store_word;
    
    FILE* Temp;
    Register = malloc(sizeof(register_file));
    init_register(Register);
    Memory = malloc(sizeof(memory));
    init_memory(Memory);
    // check flags
    for (int j = 0; j < i; j++) {
        int instruction_type = decodeInstruction(hex[j]);
        if (instruction_type == 0 || instruction_type == 63) {
            r_type* instruction = load_r_type(hex[j]);
            if (instruction->function == 63) {
                system_call(Register, Temp);
                if (strcmp(argv[2], "-s") == 0) {
                    print_register(Register);
                }
            }
            else {
                (*r_type_function[instruction->function])(instruction, Register);
                if (strcmp(argv[2], "-s") == 0) {
                    print_register(Register);
                }
            }
        }
        else if ((instruction_type == 2) || (instruction_type == 3))
        {
            j_type* instruction = load_j_type(hex[j]);
            if (instruction->opcode == 2) {
                jump(instruction, Register);
                if (strcmp(argv[2], "-s") == 0) {
                    print_register(Register);
                }
            }
            else {
                jump_and_link(instruction, Register);
                if (strcmp(argv[2], "-s") == 0) {
                    print_register(Register);
                }
            }
        }
        else
        {
            // handle mul operation
            if (instruction_type == 28) {
                r_type* instruction = load_r_type(hex[j]);
                (*r_type_function[instruction->opcode])(instruction, Register);
            }
            else {
                i_type* instruction = load_i_type(hex[j]);
                if (instruction->opcode == 1) {
                    if (instruction->rt == 1) {
                        branch_on_greater_than_equal_zero(instruction, Register);
                        if (strcmp(argv[2], "-s") == 0) {
                            print_register(Register);
                        }
                    }
                    else {
                        branch_on_less_than_zero(instruction, Register);
                        if (strcmp(argv[2], "-s") == 0) {
                            print_register(Register);
                        }
                    }
                }
                else {
                    if (instruction->opcode > 30) {
                        (*i_type_memory_function[instruction->opcode])(instruction, Register, Memory);
                        if (strcmp(argv[2], "-s") == 0) {
                            print_register(Register);
                        }
                    }
                    else {
                        (*i_type_register_function[instruction->opcode])(instruction, Register);
                        if (strcmp(argv[2], "-s") == 0) {
                            print_register(Register);
                        }
                    }
                }
            }
        }
    }
    if (strcmp(argv[2], "-r") == 0) {
        print_register(Register);
    }
}
