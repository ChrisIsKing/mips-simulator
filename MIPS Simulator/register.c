//
//  register.c
//  MIPS Simulator
//
//  Created by Christopher Clarke on 3/9/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

#include "register.h"
#define int32_t int

void print_register(register_file* Register) {
    for (int i = 0; i < 32; i++) {
        printf("Register[%i]: %i\n", i, Register->register_file[i]);
    }
    printf("Register hi = %i\n", Register->hi);
    printf("Register lo = %i\n", Register->lo);
    printf("Program Counter = %i\n", Register->pc);
}

void init_register(register_file* Register)
{
    for (int i = 0; i < 32; i++) {
        Register->register_file[i] = 0;
    }
    Register->hi = 0;
    Register->lo = 0;
    Register->$f0 = 0;
    Register->$f12 = 0;
    Register->pc = 0;
}

void init_memory(memory* Memory)
{
    for (int i = 0; i < 1000; i++) {
        Memory->address[i] = 0;
    }
}