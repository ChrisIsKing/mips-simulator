//
//  instruction.c
//  MIPS Simulator
//
//  Created by Christopher Clarke on 3/8/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

#include "instruction.h"
#include "register.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define int32_t int

//! Byte swap unsigned int
uint32_t swap_uint32( uint32_t val )
{
    val = ((val << 8) & 0xFF00FF00 ) | ((val >> 8) & 0xFF00FF );
    return (val << 16) | (val >> 16);
}

int decodeInstruction(int hex)
{
    return hex >> 26 & 63;
}

int get_rs(int hex)
{
    //int result = hex << 6;
    //result = result >> 27 & 31;
    return hex >> 21 & 31;
}

int get_rt(int hex)
{
    //int result = hex << 11;
    //result = result >> 27 & 31;
    return hex >> 16 & 31;
}

int get_rd(int hex)
{
    //int result = hex << 16;
    //result = result >> 27 & 31;
    return hex >> 11 & 31;
}

int get_function(int hex)
{
    return hex & 63;
}

int get_imm(int hex)
{
    if ((hex >> 15 & 1) == 0) {
        return hex & 65535;
    }
    else {
        return (hex & 65535) - pow(2, 16);
    }
    
}

int get_target_address(int hex)
{
    return hex & 67108863;
}

int get_shift_amount(int hex)
{
    return hex >> 6 & 31;
}

r_type* load_r_type(int hex)
{
    r_type* instruction = malloc(sizeof(r_type));
    instruction->opcode = decodeInstruction(hex);
    instruction->rs = get_rs(hex);
    instruction->rt = get_rt(hex);
    instruction->rd = get_rd(hex);
    instruction->shift_amount = get_shift_amount(hex);
    instruction->function = get_function(hex);
    return instruction;
}

i_type* load_i_type(int hex)
{
    i_type* instruction = malloc(sizeof(i_type));
    instruction->opcode = decodeInstruction(hex);
    instruction->rs = get_rs(hex);
    instruction->rt = get_rt(hex);
    instruction->imm = get_imm(hex);
    return instruction;
}

j_type* load_j_type(int hex)
{
    j_type* instruction = malloc(sizeof(j_type));
    instruction->opcode = decodeInstruction(hex);
    instruction->target_address = get_target_address(hex);
    return instruction;
}

bool add_with_overflow(r_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] > 0 && Register->register_file[instruction->rt] > INT32_MAX - Register->register_file[instruction->rs])
        return false;
    else if (Register->register_file[instruction->rs] < 0 && Register->register_file[instruction->rt] < INT32_MIN - Register->register_file[instruction->rs])
        return false;
    Register->register_file[instruction->rd] = Register->register_file[instruction->rs] + Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool add_without_overflow(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rs] + Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool add_immediate_with_overflow(i_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] > 0 && instruction->imm > INT32_MAX - Register->register_file[instruction->rs])
        return false;
    else if (Register->register_file[instruction->rs] < 0 && instruction->imm < INT32_MIN - Register->register_file[instruction->rs])
        return false;
    Register->register_file[instruction->rt] = Register->register_file[instruction->rs] + instruction->imm;
    Register->pc += 4;
    return true;
}

bool add_immediate_without_overflow(i_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rt] = Register->register_file[instruction->rs] + instruction->imm;
    Register->pc += 4;
    return true;
}

bool AND(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rs] & Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool and_immediate(i_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rt] = Register->register_file[instruction->rs] & instruction->imm;
    Register->pc += 4;
    return true;
}

bool divide_with_overflow(r_type* instruction, register_file* Register)
{
    if (((Register->register_file[instruction->rs] / Register->register_file[instruction->rt]) > INT32_MAX) || (Register->register_file[instruction->rs] / Register->register_file[instruction->rt]) < INT32_MIN) {
        return false;
    }
    Register->lo = Register->register_file[instruction->rs] / Register->register_file[instruction->rt];
    Register->hi = Register->register_file[instruction->rs] % Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool divide_without_overflow(r_type* instruction, register_file* Register)
{
    Register->lo = Register->register_file[instruction->rs] / Register->register_file[instruction->rt];
    Register->hi = Register->register_file[instruction->rs] % Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool multiply(r_type* instruction, register_file* Register)
{
    int64_t temp = (int64_t)(Register->register_file[instruction->rs] * Register->register_file[instruction->rt]);
    Register->hi = (int32_t)((temp >> 32) & 0xFFFFFFFF);
    Register->lo = (int32_t)(temp & 0xFFFFFFFF);
    Register->pc += 4;
    return true;
}

bool unsigned_multiply(r_type* instruction, register_file* Register)
{
    uint64_t temp = (uint64_t)(Register->register_file[instruction->rs] * Register->register_file[instruction->rt]);
    Register->hi = (uint32_t)((temp >> 32) & 0xFFFFFFFF);
    Register->lo = (uint32_t)(temp & 0xFFFFFFFF);
    Register->pc += 4;
    return true;
}

bool multiply_without_overflow(r_type* instruction, register_file* Register)
{
    int64_t temp = (int64_t)(Register->register_file[instruction->rs] * Register->register_file[instruction->rt]);
    Register->register_file[instruction->rd] = temp & 0xFFFFFFFF;
    Register->pc += 4;
    return true;
}



bool nor(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = ~(Register->register_file[instruction->rs] | Register->register_file[instruction->rt]);
    Register->pc += 4;
    return true;
}

bool OR(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rs] | Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool or_immediate(i_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rt] = Register->register_file[instruction->rs] | instruction->imm;
    Register->pc += 4;
    return true;
}

bool shift_left_logical(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rt] << instruction->shift_amount;
    Register->pc += 4;
    return true;
}

bool shift_left_logical_variable(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rt] << Register->register_file[instruction->rs];
    Register->pc += 4;
    return true;
}

bool shift_right_arithmetic(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = (signed)Register->register_file[instruction->rt] >> instruction->shift_amount;
    Register->pc += 4;
    return true;
}

bool shift_right_arithmetic_variable(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = (signed)Register->register_file[instruction->rt] >> Register->register_file[instruction->rs];
    Register->pc += 4;
    return true;
}

bool shift_right_logical(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rt] >> instruction->shift_amount;
    Register->pc += 4;
    return true;
}

bool shift_right_logical_variable(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rt] >> Register->register_file[instruction->rs];
    Register->pc += 4;
    return true;
}

bool subtract_with_overflow(r_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] > 0 && Register->register_file[instruction->rt] > INT32_MAX - Register->register_file[instruction->rs])
        return false;
    else if (Register->register_file[instruction->rs] < 0 && instruction->rd < INT32_MIN - Register->register_file[instruction->rs])
        return false;
    Register->register_file[instruction->rd] = Register->register_file[instruction->rs] - Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool subtract_without_overflow(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rs] - Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool Xor(r_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rd] = Register->register_file[instruction->rs] ^ Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool Xor_immediate(i_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rt] = Register->register_file[instruction->rs] ^ instruction->imm;
    Register->pc += 4;
    return true;
}

bool load_upper_immediate(i_type* instruction, register_file* Register)
{
    Register->register_file[instruction->rt] = instruction->imm;
    Register->register_file[instruction->rt] = Register->register_file[instruction->rt] << 16;
    Register->pc += 4;
    return true;
}

bool set_less_than(r_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] < Register->register_file[instruction->rt])
    {
        Register->register_file[instruction->rd] = 1;
        Register->pc += 4;
        return true;
    }
    else
    {
        Register->register_file[instruction->rd] = 0;
        Register->pc += 4;
        return true;
    }
}

bool set_less_than_unsigned(r_type* instruction, register_file* Register)
{
    if ((unsigned)Register->register_file[instruction->rs] < (unsigned)Register->register_file[instruction->rt])
    {
        Register->register_file[instruction->rd] = 1;
        Register->pc += 4;
        return true;
    }
    else
    {
        Register->register_file[instruction->rd] = 0;
        Register->pc += 4;
        return true;
    }
}

bool set_less_than_immediate(i_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] < instruction->imm)
    {
        Register->register_file[instruction->rt] = 1;
        Register->pc += 4;
        return true;
    }
    else
    {
        Register->register_file[instruction->rt] = 0;
        Register->pc += 4;
        return true;
    }
}

bool set_less_than_unsigned_immediate(i_type* instruction, register_file* Register)
{
    if ((unsigned)Register->register_file[instruction->rs] < (unsigned)instruction->imm)
    {
        Register->register_file[instruction->rt] = 1;
        Register->pc += 4;
        return true;
    }
    else
    {
        Register->register_file[instruction->rt] = 0;
        Register->pc += 4;
        return true;
    }
}

bool branch_on_equal(i_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] == Register->register_file[instruction->rt]) {
        Register->pc = Register->pc + ((int32_t)instruction->imm << 2) + 4;
        return true;
    }
    else
    {
        Register->pc = Register->pc + 4;
        return true;
    }
}

bool branch_on_greater_than_equal_zero(i_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] >= 0) {
        Register->pc = Register->pc + ((int32_t)instruction->imm << 2) + 4;
        return true;
    }
    else
    {
        Register->pc = Register->pc + 4;
        return true;
    }
}

bool branch_on_greater_than_zero(i_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] > 0) {
        Register->pc = Register->pc + ((int32_t)instruction->imm << 2) + 4;
        return true;
    }
    else
    {
        Register->pc = Register->pc + 4;
        return true;
    }
}

bool branch_on_less_than_zero(i_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] < 0) {
        Register->pc = Register->pc + ((int32_t)instruction->imm << 2) + 4;
        return true;
    }
    else
    {
        Register->pc = Register->pc + 4;
        return true;
    }
}

bool branch_on_not_equal(i_type* instruction, register_file* Register)
{
    if (Register->register_file[instruction->rs] != Register->register_file[instruction->rt]) {
        Register->pc = Register->pc + ((int32_t)instruction->imm << 2) + 4;
        return true;
    }
    else
    {
        Register->pc = Register->pc + 4;
        return true;
    }
}

bool bswap_mips(r_type* instruction, register_file* Register) {
    Register->register_file[instruction->rd] = swap_uint32(Register->register_file[instruction->rs]);
    Register->pc = Register->pc + 4;
    return true;
}

bool cmp(r_type* instruction, register_file* Register) {
    if (Register->register_file[instruction->rs] < Register->register_file[instruction->rt]) {
        Register->register_file[18] = 1;
    } else {
        Register->register_file[18] = 0;
    }
    if (Register->register_file[instruction->rs] == Register->register_file[instruction->rt]) {
        Register->register_file[19] = 1;
    } else {
        Register->register_file[19] = 0;
    }
    if (Register->register_file[instruction->rs] > Register->register_file[instruction->rt]) {
        Register->register_file[20] = 1;
    } else {
        Register->register_file[20] = 0;
    }
    if (Register->register_file[instruction->rs] <= Register->register_file[instruction->rt]) {
        Register->register_file[21] = 1;
    } else {
        Register->register_file[21] = 0;
    }
    if (Register->register_file[instruction->rs] >= Register->register_file[instruction->rt]) {
        Register->register_file[22] = 1;
    } else {
        Register->register_file[22] = 0;
    }
    if (Register->register_file[instruction->rs] != Register->register_file[instruction->rt]) {
        Register->register_file[23] = 1;
    } else {
        Register->register_file[23] = 0;
    }
    Register->pc = Register->pc + 4;
    return true;
}

bool cmpi(i_type* instruction, register_file* Register) {
    if (Register->register_file[instruction->rs] < instruction->imm) {
        Register->register_file[18] = 1;
    } else {
        Register->register_file[18] = 0;
    }
    if (Register->register_file[instruction->rs] == instruction->imm) {
        Register->register_file[19] = 1;
    } else {
        Register->register_file[19] = 0;
    }
    if (Register->register_file[instruction->rs] > instruction->imm) {
        Register->register_file[20] = 1;
    } else {
        Register->register_file[20] = 0;
    }
    if (Register->register_file[instruction->rs] <= instruction->imm) {
        Register->register_file[21] = 1;
    } else {
        Register->register_file[21] = 0;
    }
    if (Register->register_file[instruction->rs] >= instruction->imm) {
        Register->register_file[22] = 1;
    } else {
        Register->register_file[22] = 0;
    }
    if (Register->register_file[instruction->rs] != instruction->imm) {
        Register->register_file[23] = 1;
    } else {
        Register->register_file[23] = 0;
    }
    Register->pc = Register->pc + 4;
    return true;
}

bool jump(j_type* instruction, register_file* Register)
{
    Register->pc = (Register->pc & 0xF0000000) | (instruction->target_address << 2);
    return true;
}

bool jump_and_link(j_type* instruction, register_file* Register)
{
    Register->register_file[31] = Register->pc + 4;
    jump(instruction, Register);
    return true;
}

bool jump_register(r_type* instruction, register_file* Register)
{
    Register->pc = Register->register_file[instruction->rs];
    return true;
}

bool load_byte(i_type* instruction, register_file* Register, memory* Memory)
{
    Register->register_file[instruction->rt] = 0xFFFFFFF & (( 0xFF & (Memory->address[Register->register_file[instruction->rs] + instruction->imm])));
    Register->pc += 4;
    return true;
}

bool load_unsigned_byte(i_type* instruction, register_file* Register, memory* Memory)
{
    Register->register_file[instruction->rt] = ( 0xFF & (Memory->address[Register->register_file[instruction->rs] + instruction->imm]));
    Register->pc += 4;
    return true;
}

bool load_half_word(i_type* instruction, register_file* Register, memory* Memory)
{
    Register->register_file[instruction->rt] = 0xFFFFFFF & (( 0xFFFF & (Memory->address[Register->register_file[instruction->rs] + instruction->imm])));
    Register->pc += 4;
    return true;
}

bool load_unsigned_half_word(i_type* instruction, register_file* Register, memory* Memory)
{
    Register->register_file[instruction->rt] = ( 0xFFFF & (Memory->address[Register->register_file[instruction->rs] + instruction->imm]));
    Register->pc += 4;
    return true;
}

bool load_word(i_type* instruction, register_file* Register, memory* Memory)
{
    Register->register_file[instruction->rt] = ( 0xFFFFFFFF & (Memory->address[Register->register_file[instruction->rs] + instruction->imm]));
    Register->pc += 4;
    return true;
}

bool store_byte(i_type* instruction, register_file* Register, memory* Memory)
{
    Memory->address[Register->register_file[instruction->rs] + instruction->imm] = 0xFF & Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool store_half_word(i_type* instruction, register_file* Register, memory* Memory)
{
    Memory->address[Register->register_file[instruction->rs] + instruction->imm] = 0xFFFF & Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool store_word(i_type* instruction, register_file* Register, memory* Memory)
{
    Memory->address[Register->register_file[instruction->rs] + instruction->imm] = 0xFFFFFFFF & Register->register_file[instruction->rt];
    Register->pc += 4;
    return true;
}

bool system_call(register_file* Register, FILE* file)
{
    
    int arg = Register->register_file[2];
    if (arg == 1) {
        printf("%i", Register->register_file[4]);
        fflush(stdout);
        Register->pc += 4;
        return true;
    }
    else if (arg == 2) {
        printf("\n%f", Register->$f12);
        fflush(stdout);
        Register->pc += 4;
        return true;
    }
    else if (arg == 3)
    {
        printf("\n%lf", Register->$f12);
        fflush(stdout);
        Register->pc += 4;
        return true;
    }
    else if (arg == 4)
    {
        char* string = Register->register_file[4];
        printf("\n%s", string);
        fflush(stdout);
        Register->pc += 4;
        return true;
    }
    else if (arg == 5)
    {
        scanf("%i", &Register->register_file[2]);
        Register->pc += 4;
        return true;
    }
    else if (arg == 6)
    {
        float temp;
        scanf("%f", &temp);
        Register->$f0 = temp;
        Register->pc += 4;
        return true;
    }
    else if (arg == 7)
    {
        double temp;
        scanf("%lf", &temp);
        Register->$f0 = temp;
        Register->pc += 4;
        return true;
    }
    else if (arg == 8)
    {
        char* string = Register->register_file[4];
        int max = Register->register_file[5];
        fgets(string, max, stdin);
        Register->pc += 4;
        return true;
    }
    else if (arg == 9)
    {
        char* mem = malloc(sizeof(Register->register_file[4]));
        Register->register_file[2] = &mem;
        Register->pc += 4;
        return true;
    }
    else if (arg == 10)
    {
        exit(10);
    }
    else if (arg == 11)
    {
        char c = Register->register_file[4];
        printf("%c", c);
        fflush(stdout);
        Register->pc += 4;
        return true;
    }
    else if (arg == 12)
    {
        char c;
        scanf("%c", &c);
        Register->register_file[4] = c;
        Register->pc += 4;
        return true;
    }
    else if(arg == 13)
    {
        char* filename = Register->register_file[4];
        char* mode = Register->register_file[6];
        char* flag = Register->register_file[5];
        file = fopen(filename, mode);
        if (file == NULL) {
            printf("Invalid Filename");
            exit(100);
        }
        Register->register_file[2] = file;
        return true;
    }
    else if (arg == 14)
    {
        file = Register->register_file[4];
        char* string = Register->register_file[5];
        fgets(string, Register->register_file[6], file);
        Register->register_file[2] = strlen(string);
        return true;
    }
    else if (arg == 15)
    {
        file = Register->register_file[4];
        char* string = Register->register_file[5];
        fwrite(string, 1, Register->register_file[6], file);
        Register->register_file[2] = Register->register_file[6];
        return true;
    }
    else if (arg == 16)
    {
        file = Register->register_file[4];
        fclose(file);
        return true;
    }
    else if (arg == 17)
    {
        exit(Register->register_file[4]);
    }
    return false;
}