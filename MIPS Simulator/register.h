//
//  register.h
//  MIPS Simulator
//
//  Created by Christopher Clarke on 3/9/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

#ifndef register_h
#define register_h

#include <stdio.h>
#define int32_t int

/*typedef struct register_file {
    int $zero;
    int $at;
    int $v0;
    int $v1;
    int $a0;
    int $a1;
    int $a2;
    int $a3;
    int $t0;
    int $t1;
    int $t2;
    int $t3;
    int $t4;
    int $t5;
    int $t6;
    int $t7;
    int $s0;
    int $s1;
    int $s2;
    int $s3;
    int $s4;
    int $s5;
    int $s6;
    int $s7;
    int $t8;
    int $t9;
    int $k0;
    int $k1;
    int $gp;
    int $sp;
    int $fp;
    int $ra;
}register_file;*/

typedef struct register_file {
    int register_file[32];
    int hi;
    int lo;
    int pc;
    float $f12;
    float $f0;
}register_file;

typedef struct memory {
    int32_t address[1000];
}memory;
/*#define $zero reg[0]
#define $at reg[1]
#define $v0 reg[2]
#define $v1 reg[3]
#define $a0 reg[4]
#define $a1 reg[5]
#define $a2 reg[6]
#define $a3 reg[7]
#define $t0 reg[8]
#define $t1 reg[9]
#define $t2 reg[10]
#define $t3 reg[11]
#define $t4 reg[12]
#define $t5 reg[13]
#define $t6 reg[14]
#define $t7 reg[15]
#define $s0 reg[16]
#define $s1 reg[17]
#define $s2 reg[18]
#define $s3 reg[19]
#define $s4 reg[20]
#define $s5 reg[21]
#define $s6 reg[22]
#define $s7 reg[23]
#define $t8 reg[24]
#define $t9 reg[25]
#define $k0 reg[26]
#define $k1 reg[27]
#define $gp reg[28]
#define $sp reg[29]
#define $fp reg[30]
#define $ra reg[31]*/

void print_register(register_file*);
void init_register(register_file*);
void init_memory(memory*);

#endif /* register_h */
